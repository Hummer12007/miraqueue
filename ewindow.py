from app import app, db, lman, mqtt
from model import *
from flask import jsonify, request, g, render_template, redirect, url_for, flash
from flask_login import login_required, current_user, login_user, logout_user
from werkzeug.security import check_password_hash
from time import time
from util import try_delete
import json

@app.before_request
def before_request():
    if not request.path.startswith('/employee') or not hasattr(current_user, 'wrapper'):
        return
    g.u = current_user.wrapper.employee

@app.route('/employee/login', methods=['GET','POST'])
def emp_login():
    if request.method == 'GET':
        return render_template('emp_login.html')
    username = request.form['username']
    password = request.form['password']
    user = Employee.query.filter_by(username=username).first()
    if user is None:
        flash('Invalid username', 'warning')
        return redirect(url_for('emp_login'))
    if not check_password_hash(user.phash, request.form['password']):
        flash('Invalid password', 'warning')
        return redirect(url_for('emp_login'))
    login_user(user, remember=True)
    flash('Logged in')
    return redirect(url_for('emp_main'))

@app.route('/employee')
@login_required
def emp_main():
    state = int(request.args['state']) if 'state' in request.args else 0
    client = None
    if state == 0:
        return render_template('beta_window.html', state=state, employee=g.u)
    elif state == 1:
        client = Consumer.query.get(request.args['client_id'])
        if not client:
            client = {'id': -1}
        return render_template('beta_window.html', state=state, client=client, cqn=request.args['cqn'], employee=g.u, qid=request.args['qid'])
    elif state == 2:
        client = Consumer.query.get(request.args['client_id'])
        if not client:
            client = {'id': -1}
        return render_template('beta_window.html', state=state, client=client, employee=g.u)

@app.route('/employee/done')
@login_required
def emp_done():
    return redirect(url_for('emp_main'))

@app.route('/employee/next_client')
@login_required
def emp_next_client():
    q = g.u.queue
    cq = ConsumerQueues.query.filter_by(queue_id=q.id).first()
    if not cq:
        return redirect(url_for('emp_main'))
    tid = cq.ticket_id
    cid = cq.consumer_id
    wait_duration = time() - cq.start_time
    ql = QueueLog(q.id, tid, cid, wait_duration)
    number = cq.queue_number
    db.session.delete(cq)
    db.session.add(ql)
    db.session.commit()
    mqtt.publish(str(q.id), json.dumps({'operation':'call', 'queue_number':number, 'window':g.u.window}))
    mqtt.publish('user' + str(cid), json.dumps({'message_type':'come_to_window', 'window': g.u.window, 'ticket_id': tid}))
    data = HelperData.query.filter((HelperData.user_id==cid)&(HelperData.queue_id==q.id)).first()
    if data:
        data.window = g.u.window
        db.session.add(data)
        db.session.commit()
    return redirect(url_for('emp_main') + '?client_id={}&state={}&cqn={}&qid={}'.format(cid, 1, number, q.id))

@app.route('/employee/process_client')
@login_required
def emp_process_client():
    cqn = int(request.args['cq_number'])
    qid = int(request.args['qid'])
    cid = request.args['client_id']
    mqtt.publish(str(qid), json.dumps({'operation':'remove', 'queue_number':cqn}))
    data = HelperData.query.filter((HelperData.user_id==cid)&(HelperData.queue_id==qid)).first()
    try_delete(data)
    return redirect(url_for('emp_main') +'?state={}&client_id={}'.format(2, cid))

@app.route('/employee/skip_client')
@login_required
def emp_skip_client():
    cqn = int(request.args['cq_number'])
    qid = int(request.args['qid'])
    cid = request.args['client_id']
    mqtt.publish(str(qid), json.dumps({'operation':'remove', 'queue_number':cqn}))
    data = HelperData.query.filter((HelperData.user_id==cid)&(HelperData.queue_id==qid)).first()
    try_delete(data)
    return redirect(url_for('emp_main'))
