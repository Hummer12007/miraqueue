from app import app, db, lman, mqtt
from model import *
from flask import jsonify, request, g, render_template, redirect, url_for, flash
from flask_login import login_required, current_user, login_user, logout_user
from werkzeug.security import check_password_hash
from time import time
import json

@app.route('/tv/<qid>')
def tv(qid):
    cqs = ConsumerQueues.query.filter_by(queue_id=qid)
    ret = json.dumps([{'num': cq.queue_number, 'ready': False} for cq in cqs])
    return render_template('tv.html', qid=qid, data=ret)

@app.route('/terminal/<oid>')
def terminal(oid):
    org = Organization.query.get_or_404(oid)
    o = json.loads(org.tickettree)
    child_path = ''
    if 'child_path' in request.args:
        child_path = request.args['child_path']
        indices = [int(i) for i in child_path.split(',') if i != '']
        for i in indices:
            o = o[i]['children']
    h = '<div class="container">'
    if child_path != '':
        h += ('<div class="child-node backCard"><a class="cardLabel" href="{}">Повернутися</a></div>'
            .format('/terminal/{}?child_path={}'
                    .format(oid, ','.join(i for i in child_path.split(',')[1:] if i != ''))))
    for i, child in enumerate(o):
        s = '<div class="child-node"><a class="cardLabel" href="{}">{}</a></div>'
        if 'children' in child:
            h += s.format('/terminal/{}?child_path={}'.format(oid, (child_path+','+str(i)).lstrip(',')), child['name'])
        else:
            h += '<div class="child-node"><a class="cardLabel" href="{}">{}</a></div>'.format('/terminal/{}/ticket/{}'.format(oid, child['ticket_id']), child['name'])
    h += '</div>'
    return render_template('terminal.html', body=h)

@app.route('/terminal/<oid>/ticket/<tid>')
def term_ticket(oid, tid):
    ticket = Ticket.query.get(tid)
    q = ticket.queue
    cq = ConsumerQueues(q.id, tid, -1, int(time()))
    db.session.add(cq)
    db.session.commit()
    mqtt.publish(str(q.id), json.dumps({'operation':'add', 'queue_number':cq.queue_number}))
    flash('Added you to the queue!')
    return redirect('/terminal/'+str(oid))
