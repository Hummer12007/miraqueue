from app import *
from model import *
from flask import redirect, render_template

@app.route('/forms')
def forms():
    return render_template('create.html')

@app.route('/createorg', methods=['POST'])
def createorg():
    o = Organization(
            request.form['name'],
            request.form['description'],
            request.form['tickettree'],
            request.form['username'],
            request.form['password'])
    db.session.add(o)
    db.session.commit()
    return redirect('/forms')

@app.route('/createticket', methods=['POST'])
def createticket():
    t = Ticket(
            int(request.form['orgid']),
            request.form['name'],
            request.form['description'],
            int(request.form['base_duration']))
    db.session.add(t)
    db.session.commit()
    return redirect('/forms')

@app.route('/createemp', methods=['POST'])
def createemployee():
    e = Employee(
            int(request.form['orgid']),
            request.form['name'],
            int(request.form['window']),
            request.form['username'],
            request.form['password'])
    db.session.add(e)
    db.session.commit()
    return redirect('/forms')
