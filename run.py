from app import *
import model
import auth
import api
import ewindow
import clients
import forms
import optparse

def run(app, default_host='0.0.0.0', default_port='5000'):
    parser = optparse.OptionParser()
    parser.add_option("-H", "--host", default=default_host)
    parser.add_option("-p", "--port", default=default_port)
    parser.add_option("-d", "--debug", action="store_true", dest="debug")
    options, _ = parser.parse_args()
    app.run(debug=options.debug, host=options.host, port=int(options.port))


if (__name__ == '__main__'):
    db.create_all()
    run(app)
