from app import db

def try_delete(foo):
    if foo:
        db.session.delete(foo)
        db.session.commit()
