from flask_login import UserMixin
from werkzeug.security import generate_password_hash, gen_salt

from app import db, lman

@lman.user_loader
def load_user(id):
    u = Consumer.query.get(int(id))
    u.wrapper = UserWrapper(id)
    return u

class UserWrapper():
    def __init__(self, id):
        self.consumer = Consumer.query.get(int(id))
        self.admin = Organization.query.get(int(id))
        self.employee = Employee.query.get(int(id))


class Consumer(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    phone_number = db.Column(db.Integer)
    name = db.Column(db.String(128))
    phash = db.Column(db.String(128))
    sessions = db.relationship('Session', backref='user', lazy=True)

    def __init__(self, phone_number, name, password):
        self.phone_number = phone_number
        self.name = name
        self.phash = generate_password_hash(password, 'sha256')

class Organization(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    name = db.Column(db.String(128))
    description = db.Column(db.Text)
    tickettree = db.Column(db.Text)
    username = db.Column(db.String(128))
    phash = db.Column(db.String(128))

    def __init__(self, name, description, tickettree, username, password):
        self.name = name
        self.description = description
        self.tickettree = tickettree
        self.username = username
        self.phash = generate_password_hash(password, 'sha256')

queue_tickets = db.Table('queue_tickets',
    db.Column('queue_id', db.Integer, db.ForeignKey('queue.id'), primary_key=True),
    db.Column('ticket_id', db.Integer, db.ForeignKey('ticket.id'), primary_key=True)
)

queue_employees = db.Table('queue_employees',
    db.Column('queue_id', db.Integer, db.ForeignKey('queue.id'), primary_key=True),
    db.Column('employee_id', db.Integer, db.ForeignKey('employee.id'), primary_key=True)
)

class Queue(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'))
    wait_duration = db.Column(db.Integer)
    employees = db.relationship('Employee', secondary=queue_employees, lazy='subquery',
        backref=db.backref('queue', lazy=True, uselist=False))
    tickets = db.relationship('Ticket', secondary=queue_tickets, lazy='subquery',
        backref=db.backref('queue', lazy=True, uselist=False))

    def __init__(self, organization_id, wait_duration):
        self.organization_id = organization_id
        self.wait_duration = wait_duration

class Ticket(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'))
    name = db.Column(db.String(128))
    description = db.Column(db.Text)
    base_duration = db.Column(db.Integer)

    def __init__(self, organization_id, name, description, base_duration):
        self.organization_id = organization_id
        self.name = name
        self.description = description
        self.base_duration = base_duration

class Employee(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'))
    name = db.Column(db.String(128))
    window = db.Column(db.Integer)
    username = db.Column(db.String(128))
    phash = db.Column(db.String(128))

    def __init__(self, organization_id, name, window, username, password):
        self.organization_id = organization_id
        self.name = name
        self.window = window
        self.username = username
        self.phash = generate_password_hash(password, 'sha256')

class QueueLog(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    queue_id = db.Column(db.Integer, db.ForeignKey('queue.id'))
    ticket_id = db.Column(db.Integer, db.ForeignKey('ticket.id'))
    consumer_id = db.Column(db.Integer, db.ForeignKey('consumer.id'))
    wait_duration = db.Column(db.Integer)

    def __init__(self, queue_id, ticket_id, consumer_id, wait_duration):
        self.queue_id = queue_id
        self.ticket_id = ticket_id
        self.consumer_id = consumer_id
        self.wait_duration = wait_duration

class ConsumerQueues(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    queue_id = db.Column(db.Integer, db.ForeignKey('queue.id'))
    ticket_id = db.Column(db.Integer, db.ForeignKey('ticket.id'))
    consumer_id = db.Column(db.Integer, db.ForeignKey('consumer.id'))
    queue_number = db.Column(db.Integer)
    start_time = db.Column(db.Integer)

    @property
    def queue_number(self):
        return self.id % 900 + 100

    def __init__(self, queue_id, ticket_id, consumer_id, start_time):
        self.queue_id = queue_id
        self.ticket_id = ticket_id
        self.consumer_id = consumer_id
        self.start_time = start_time


class Session(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('consumer.id'))
    session_id = db.Column(db.String(64))

    def __init__(self, user_id):
        self.user_id = user_id
        self.session_id = gen_salt(48)

class HelperData(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.Integer, db.ForeignKey('consumer.id'))
    ticket_id = db.Column(db.Integer, db.ForeignKey('ticket.id'))
    queue_id = db.Column(db.Integer, db.ForeignKey('queue.id'))
    confirmed = db.Column(db.Boolean)
    window = db.Column(db.Integer)

    def __init__(self, user_id, ticket_id, queue_id):
        self.user_id = user_id
        self.ticket_id = ticket_id
        self.queue_id = queue_id
        self.confirmed = False
        self.window = -1
