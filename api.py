from app import app, db, mqtt
from model import *
from flask import jsonify, request, g
from auth import auth_required
from werkzeug.security import check_password_hash
import json
from time import time

@app.route('/api/login', methods=['POST'])
def login():
    if not ('username' in request.form and 'password' in request.form):
        return jsonify(error='Not enough login data')
    phone_number = request.form.get('username')
    passw = request.form.get('password')
    user = Consumer.query.filter_by(phone_number=phone_number).first()
    if not user or not check_password_hash(user.phash, passw):
        return jsonify(error='Wrong user or password')
    sess = Session(user.id)
    db.session.add(sess)
    db.session.commit()
    return jsonify(access_token=sess.session_id, user_id=user.id)

@app.route('/api/organizations')
@auth_required
def orgs():
    organizations = Organization.query.all()
    ret = {'organizations':[]}
    for org in organizations:
        ret['organizations'].append({'id':org.id, 'name':org.name, 'description':org.description, 'tickettree':org.tickettree, 'username':org.username})

    """
    o = Organization('Адміністрація', 'Справки та свідоцтва', '[{"name": "Видача справок","children":[ {"name": "Справка 1", "ticket_id": 1},{"name": "Справка 1", "ticket_id": 2}] },{"name": "Видача свідотцв","children":[ {"name": "Сві 1", "ticket_id": 3},{"name": "Сві 2", "ticket_id": 4} ]}]', 'org1', 'org1')
    db.session.add(o)
    db.session.commit()
    t1 = Ticket('1', 'Справка 1', 'Видача справки 1', '10')
    t2 = Ticket('1', 'Справка 2', 'Видача справки 2', '10')
    t3 = Ticket('1', 'Сві 1', 'Видача сві 1', '30')
    t4 = Ticket('1', 'Сві 2', 'Видача сві 2', '30')
    db.session.add(t1)
    db.session.add(t2)
    db.session.add(t3)
    db.session.add(t4)
    db.session.commit()
    """
    return json.dumps(ret)


@app.route('/api/organizations/search')
@auth_required
def org_search():
    looking_for = request.params['query']
    organizations = Organization.query.filter((Organization.name.ilike(looking_for)) | (Organization.description.ilike(looking_for)))
    ret = {'organizations':[]}
    for org in organizations:
        ret['organizations'].append({'id':org.id, 'name':org.name, 'description':org.description, 'tickettree':org.tickettree, 'username':org.username})
    return json.dumps(ret)


@app.route('/api/organization/<id>')
@auth_required
def org_info(id):
    org = Organization.query.get(id)
    if not org:
        return jsonify(error='Not found')
    return jsonify(id=org.id, name=org.name, description=org.description, tickettree=org.tickettree)


@app.route('/api/organization/<id>/tickets')
@auth_required
def org_tickets(id):
    tickets = Ticket.query.filter_by(organization_id=id)
    if not tickets:
        return jsonify(error='Not found')
    ret = {'tickets':[]}
    for ticket in tickets:
        ret['tickets'].append({'id':ticket.id, 'organization_id':ticket.organization_id, 'name':ticket.name, 'description':ticket.description, 'base_duration':ticket.base_duration})
    return json.dumps(ret)


@app.route('/api/organization/<id>/tickets/search')
@auth_required
def ticket_search(id):
    looking_for = request.params['query']
    tickets = Ticket.query.filter(((Ticket.organization_id == id) & (Ticket.name.ilike(looking_for) | Ticket.description.ilike(looking_for))))
    if not tickets:
        return jsonify(error='Not found')
    ret = {'tickets':[]}
    for ticket in tickets:
        ret['tickets'].append({'id':ticket.id, 'organization_id':ticket.organization_id, 'name':ticket.name, 'description':ticket.description, 'base_duration':ticket.base_duration})
    return json.dumps(ret)


@app.route('/api/ticket/<id>')
@auth_required
def ticket_info(id):
    ticket = Ticket.query.get(id)
    if not ticket:
        return jsonify(error='Not found')
    cq = ConsumerQueues.query.filter((ConsumerQueues.queue_id==ticket.queue.id)&(ConsumerQueues.consumer_id==g.current_user.id)).first()
    ce = cq == None
    cl = not ce
    return jsonify(id=ticket.id, name=ticket.name, description=ticket.description, base_duration=ticket.base_duration, can_enter=ce, can_leave=cl)


@app.route('/api/queue/<id>')
@auth_required
def queue_info(id):
    q = Queue.query.get(id)
    if not q:
        return jsonify(error='Not found')
    return jsonify(id=q.id, organization_id=q.organization_id, wait_duration=q.wait_duration, employees=q.employees, tickets=q.tickets)

@app.route('/api/ticket/<id>/enter')
@auth_required
def enter_ticket(id):
    ticket = Ticket.query.get(id)
    if not ticket:
        return jsonify(error='Not found')
    q = ticket.queue
    cq = ConsumerQueues.query.filter((ConsumerQueues.queue_id==q.id)&(ConsumerQueues.consumer_id==g.current_user.id)).first()
    if cq:
        return jsonify(error='Already in queue')
    cq = ConsumerQueues(q.id, ticket.id, g.current_user.id, int(time()))
    db.session.add(cq)
    db.session.commit()
    mqtt.publish(str(q.id), json.dumps({'operation':'add', 'queue_number':cq.queue_number}))
    return jsonify(status='success')

@app.route('/api/ticket/<id>/leave')
@auth_required
def leave_ticket(id):
    ticket = Ticket.query.get(id)
    if not ticket:
        return jsonify(error='Not found')
    q = ticket.queue
    cq = ConsumerQueues.query.filter((ConsumerQueues.queue_id==q.id)&(ConsumerQueues.consumer_id==g.current_user.id)).first()
    if not cq:
        return jsonify(error='Not in queue')
    db.session.delete(cq)
    db.session.commit()
    mqtt.publish(str(q.id), json.dumps({'operation':'remove', 'queue_number':cq.queue_number}))
    return jsonify(status='success')

def consumer_index_in_queue(cid, qid, tid):
    total = 0
    i = 0
    for cqq in ConsumerQueues.query.filter_by(queue_id=qid):
        t = Ticket.query.get(cqq.ticket_id)
        i = i + 1
        if cqq.id == g.current_user.id:
            break
        total += t.base_duration
    data = HelperData.query.filter((HelperData.user_id==g.current_user.id)&(HelperData.queue_id==qid)).first()
    must_enter = False
    if not data:
        if i < 2:
            data = HelperData(g.current_user.id, tid, qid)
            db.session.add(data)
            db.session.commit()
        else:
            need_confirmation = '0'
    if data:
        need_confirmation = '2' if data.confirmed else '1'
        if data.window > 0:
            must_enter=True
    w = -1 if not data else data.window
    return i, ConsumerQueues.query.filter((ConsumerQueues.queue_id==qid)).count(), total / len(Queue.query.get(qid).employees), need_confirmation, must_enter, w

@app.route('/api/ticket/<id>/status')
@auth_required
def ticket_status(id):
    ticket = Ticket.query.get(id)
    if not ticket:
        return jsonify(error='Not found')
    q = ticket.queue
    cq = ConsumerQueues.query.filter((ConsumerQueues.queue_id==q.id)&(ConsumerQueues.consumer_id==g.current_user.id)).first()
    if not cq:
        return jsonify(error='Not in queue')
    idx, le, est, nc, me, w = consumer_index_in_queue(g.current_user.id, q.id, id)
    return jsonify(position=idx, total=le, estimation=est, must_enter=me, need_confirmation=nc, window=w)

@app.route('/api/ticket/<id>/confirm')
@auth_required
def confirm_ticket(id):
    ticket = Ticket.query.get(id)
    if not ticket:
        return jsonify(error='Not found')
    q = ticket.queue
    data = HelperData.query.filter((HelperData.user_id==g.current_user.id)&(HelperData.queue_id==q.id)).first()
    if not data:
        return jsonify(error='Not found')
    data.confirmed = True
    db.session.add(data)
    db.session.commit()
    return jsonify(status='success')

@app.route('/api/ticket/queues')
@auth_required
def my_queues():
    cq = ConsumerQueues.query.filter_by(consumer_id=g.current_user.id)
    ret = []
    for q in ConsumerQueues.query.filter_by(consumer_id=g.current_user.id):
        idx, le, est, nc, me, w = consumer_index_in_queue(g.current_user.id, q.queue_id, q.ticket_id)
        t = Ticket.query.get(q.ticket_id)
        ret.append({'ticket_id': q.ticket_id, 'queue_id': q.queue_id, 'name': t.name, 'position':le, 'total':le, 'estimation': est, 'must_enter': me, 'need_confirmation': nc, 'window': w})
    for q in HelperData.query.filter_by(user_id=g.current_user.id):
        if q.window < 0:
            continue
        t = Ticket.query.get(q.ticket_id)
        ret.append({'ticket_id': q.ticket_id, 'queue_id': q.queue_id, 'name': t.name, 'position':ConsumerQueues.query.filter_by(queue_id=q.queue_id).count(), 'total':ConsumerQueues.query.filter_by(queue_id=q.queue_id).count(), 'estimation': 0, 'must_enter': True, 'need_confirmation': '2', 'window': q.window})
    return json.dumps(ret)

